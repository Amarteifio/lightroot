# the tree code - focus on this now
#also more image statistics and different strategies
from scipy.spatial import KDTree as scKDTree
import pandas as pd 
import numpy as np
from numba import jit
from itertools import combinations

class tpctree(object):
    def __init__(self, data,options={}):
        ##init
        self._t = 0 
        self.data = None
        if data is not None:
            data.index.name = "key"
            self.data = data.reset_index()
            self.data["t"] = self._t#we do not trust any t passed in but we should not really overwrite it - think about this
        self._last_transform = None
        self._detect_ratio = []
        #could take any of these from options
        #10 seems to small and 20 too big - double counting and also an opposite direction in subtle movements on boundary to admit debris
        self._epsilon = 15 if "epsilon" not in options else options["epsilon"]
        self._columns = ["x","y","z"]
        self._transform_priors = []
        self._transformer = tpctree.__default_transformer__
        #self._sampler = tpctree.constellation_sampler#simple_sampler
        self._sampler = tpctree.simple_sampler
        self._stats = {}
      
    def __getitem__(self, k): 
        if self.data is None: return None
        return self.data[self.data["t"]==k]
    
    def to_life_matrix(self):   return None
    
    @property
    def stats(self): return self._stats
    
    @property
    def blobs(self):
        """Last known t, return slice"""
        return self[self._t]
    
    @property
    def prev_blobs(self):
        """Last known t, return slice for last t"""
        if self._t ==0:return None
        return self[self._t-1]
    
    #####TRANSFORMATIONS#######    
    #@jit
    def _get_transforms_(self,t1,t2):
        for v in self._sampler(t1[self._columns]):
            for w in self._sampler(t2[self._columns]):
                yield self._transformer(w,v)
    
    def find_transform(X,Y): 
        """
        Given two matrices, try to compute the transformation
        """
        pad = lambda x: np.hstack([x, np.ones((x.shape[0], 1))])
        unpad = lambda x: x[:,:-1]
        A, res, rank, s = np.linalg.lstsq(pad(X), pad(Y),rcond=None)
        transform = lambda x: unpad(np.dot(pad(x), A))
        return transform
    
    def find_translation(X,Y): 
        """Given two matrices, compute the simple translation
           it as assumed the point subsets represent likely congruences and the mean translation vector us used
        """
        mean_vec = np.array(Y)-np.array(X)
        if len(mean_vec.shape) == 1:  mean_vec = np.stack([mean_vec])
        mean_vec = np.stack([mean_vec.mean(axis=0)])
        return tpctree.translation_transform_for(mean_vec)
    
    def translation_transform_for(v):
        """
        Generate a simple translation transform for a vector that represents an average displacement
        This is a fall-back for the more generalized transform
        """
        def translation_matrix():
            Q = np.eye(v.shape[1]+1)
            Q[:v.shape[1],-1] = np.array(v)
            return np.matrix(Q)
        
        def translation_transform(m):
            m = np.matrix(np.hstack((m,np.ones((len(m),1)))))
            A = translation_matrix()
            m = A * m.T
            return m.T[:,:-1]
        
        #return the transform functon
        return translation_transform
    
    def __default_transformer__(X,Y):
        try:   
            #print("returning reg transform")
            return tpctree.find_transform(Y,X)
        except:  return tpctree.find_translation(Y,X)
    
    def best_transform(self, collection):
        """
        Simply return the transform that minimises the objective with respect to the data
        The data will be the point cloud at time [t] and [t-1]
        """
        best_tr, best_score = None, self._epsilon+1
        #print(len(collection),"transforms")
        for tr in collection:
            if tr is None: continue
            score = self.apply_transform(tr,False)
            if score <= best_score: 
                best_tr , best_score= tr,score
        return best_tr
    #####END TRANSFORMATIONS#######
        
    #def __eps_from_min_intertarget_distance__(self):
    #    #for each point in target, find the smallest distance between any two points
    #    #half this vaue and take the min of the max_epsilon (bio-feasible) and this value
    #    #rationale: adapt to the data up to a point. if we have some points very close together then we got the bio wrong or there is noise
        
    def update(self, data,t=None): #unless t is given, auto-inc
        """
        Update the tracker with new points
        """
        if data is None: return None
        #increment the flock
        self._t += 1
        data.loc[:,'t']  = self._t
        self.data = pd.concat([self.data, data])
        #some local vars
        t,new_transforms = self._t, []
        #get all transforms generated from the data
        for tr in self._get_transforms_(self[t], self[t-1]):
            new_transforms.append(tr)
        #add to the list, the list of priors and include the last chosen (best) transform
        for tr in self._transform_priors + [self._last_transform]: 
            new_transforms.append(tr)    
        #select the best of the transforms by evalaution against the data and save the best
        self._last_transform = self.best_transform(new_transforms)
        #apply the best transform and assign ids
        self.apply_transform(self._last_transform)
        #return the state
        return self.blobs #here we only return the latest info
    
    def __normed_distance__(self,D):
        """
        simple helper methods to replace infinity values by the model worst-score
        """
        D[(np.isinf(D)) ] = self._epsilon+1
        return D
    
    def __validate_ids__(self,res):
        """
        If the result is infinite, there is no neighbour within the ball of radius epsilon
        In such cases we should set the id to newly generated ids TODO
        """
        next_key = int(self.data.key.max() + 1)
        ids = res[1]
        invalid_mask = np.where(res[0]>=self._epsilon+1)[0]
        valid_mask = np.where(res[0]<self._epsilon+1)[0]
        
        self._detect_ratio = [len(valid_mask),len(invalid_mask)]
        self._stats["detections ratio"]= self._detect_ratio
        
        num_orphans = len(invalid_mask) 
        #for the valid ids, take the key for them (off the prev)
        ids[valid_mask] = self[self._t-1].iloc[ids[valid_mask]].key
        #for the none-valid ids, generate new keys
        newids = list(range(next_key, next_key+num_orphans))
        ids[invalid_mask ] = newids
        return np.array(ids,np.int)
    
    def apply_transform(self, tr, accept_pair=True):
        #get the actual frame-frame data points
        X,Y = self[self._t-1],self[self._t]
        #compute the transfomration from Y-> Back to X (Ys are the ones that need id pairings)
        #make sure that we compute the transform that way too
        TR = tr(Y[self._columns].as_matrix())
        #index Y, i.e. the target vector - a suitable result from this is the pairing
        #we do not deal with marriage problem and simply assign closest - could improve this but fine based on our data
        target = scKDTree(X[self._columns].as_matrix())
        #get the first nearest neighbour within threshold
        res=target.query(TR, k=1,distance_upper_bound=self._epsilon)
        #the score is the bounded distance - see called function - kdtree returns vector [distance, ids]
        score = self.__normed_distance__(res[0]).mean().round(4)
        #assign ids
        if accept_pair:
            self._temp_res = np.array(res)
            #get the keys corresponding to the integer index returned from the kdtree
            self.data.loc[self.data["t"] == self._t, "key"] = self.__validate_ids__(res)
            #assign epsilons
            self.data.loc[self.data["t"] == self._t, "epsilon"] = self.__normed_distance__(res[0])
            #should always be possible to either assign or generate - null key not acceptable
            self.data.key = self.data.key.astype(int)
        #return score of this transform
        return score
    
    ###Samplers###
    def simple_sampler(df,columns=["x", "y", "z"]):  
        """simply sample all the points"""
        return df[columns].as_matrix()

    
    def constellation_sampler(df, k=3, N=200,min_angle=20,min_length=20):
        """
        Find constellations - parameters are
        k: number of points in the constellation
        N:Number of random constellations to sample
        min_angle: the min allowed angle between two vectors
        min_length: the min alowed vector length
        """
        ps = [v for v in df.as_matrix()]

        def _validate_(ps):
            for a in utils.vector_lengths(ps):
                if a < min_length:return False
            for a in utils.vector_inter_angles(ps):
                if a < min_angle:return False        
            return True

        perms = list(combinations(ps,k))
        #perms = np.random.choice(perms, len(perms))

        counter = 0
        for i in np.random.choice(range(len(perms)),len(perms)):
            item = np.stack(list(perms[i]))
            #print(item)
            if _validate_(item): 
                counter += 1
                yield utils.sort_constellation(item)
                if counter == N:return
                
    def show_projection_callback(df,tr,r=30):
        from matplotlib import pyplot as plt
        import math

        projected = tr(df[["x", "y", "z"]])
        projected = pd.DataFrame(projected, columns=["x","y","z"])
        def _call(ax): 
            area = 2*math.pi*r
            print(area)
            ax.scatter(x=df.x, y=df.y, c='r', s=30,marker='x', label="objects at time t+1")
            ax.scatter(x=pj.x, y=pj.y, facecolors='none', edgecolors='b', s=area, label='projected')      
            plt.legend(loc=4, fontsize=20)
        return _call
